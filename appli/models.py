from flask_sqlalchemy import SQLAlchemy
import logging as lg
from .views import app

db = SQLAlchemy(app)

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    nom_crypto = db.Column(db.String(255), unique=True, nullable=False)
    quantite_achete = db.Column(db.Float, nullable=False)    
    quantite_vendu = db.Column(db.Float, nullable=False)
    quantite_actuel = db.Column(db.Float, nullable=False)
    prix_achat = db.Column(db.Float, nullable=False)
    prix_vente = db.Column(db.Float, nullable=False)
    gains_argent = db.Column(db.Float, nullable=False)

    def __init__(self, nom_crypto, quantite_achete, quantite_vendu, quantite_actuel, prix_achat, prix_vente, gains_argent):
        self.nom_crypto = nom_crypto
        self.quantite_achete = quantite_achete
        self.quantite_vendu = quantite_vendu
        self.quantite_actuel = quantite_actuel
        self.prix_achat = prix_achat
        self.prix_vente = prix_vente
        self.gains_argent = gains_argent

def init_db():
    db.drop_all()
    db.create_all()
    db.session.add(User("BTC (Bitcoin)", 10, 2, 8,1.6, 6, 4.4,))
    db.session.commit()
    lg.warning('Database initialized!')