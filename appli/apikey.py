from requests import Request, Session
import pandas as pd
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json

api_root = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/trending/most-visited'

parameters = {
  'start':'1',
  'limit':'1050',
  'convert':'EUR'
}

headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': '3bb2586d-9008-49f7-99a0-eda441f1ad99',
}

session = Session()
url_api = session.headers.update(headers)
print("url_api" + url_api)

def get_dpe_from_url(api_root):

  try:
    response = session.get(api_root, params=parameters)
    data = json.loads(response.text)

    print("c'est une réussite")
    print(data)

    df = pd.json_normalize(data["results"])

  except (ConnectionError, Timeout, TooManyRedirects) as e:
    print("erreur : ")
    print(e)

  return df

df = get_dpe_from_url(url_api)
df.head(50)
